from typing import List

class Solution:
    def countries_count(self, A: List[List[int]]) -> int:
        B:  List[List[bool]] = [[True for row in A[0]] for column in A] #sets True to the new List with same shape with A
        n_of_countries: int = 0
        #iterate over all indicies
        for row in range(len(A)):
            for column in range(len(A[row])):
                if not B[row][column]:
                # n_of_countries should be incremented only if it has not been visited or doesn't have a neighbour with same color
                # in other words, it still equals to True, since every visited and same color countries(neighbours) 
                # are assigned to False, so before incresing country count, every time value of B[row][column] should be checked, 
                # subsequently, if the value is not equal to True it should skip and continue with the next index
                    continue
                n_of_countries += 1 # count of True
                Solution().neighbours(A[row][column], B, A, row, column)
        return n_of_countries
    
    def neighbours(self, val: int, B: List[List[bool]], A: List[List[int]], row: int, column: int):
        # to be a valid matrix and neighbour and not be out of any range, the following 4 conditions should be met
        if row < 0 or row >= len(A):
            return None
        if column < 0 or column >= len(A[0]):
            return None 
        if not B[row][column] :
            return None 
        if A[row][column] != val:
            return None
            
        #if a valid matrix, then check the neighbourhoods to know whether they are belonging to the same country or not
        #if no neighbour exists, recursion should be broken

        B[row][column] = False #visited element
        Solution().neighbours(val, B, A, row + 1, column)
        Solution().neighbours(val, B, A, row - 1, column)
        Solution().neighbours(val, B, A, row, column - 1)
        Solution().neighbours(val, B, A, row, column + 1)


if __name__ == "__main__":
    A = [
            [5,4,4],
            [4,3,4],
            [3,2,4],
            [2,2,2],
            [3,3,4],
            [1,4,4],
            [4,1,1]
        ]
print("The number of countries for the given matrix is: ",Solution().countries_count(A))