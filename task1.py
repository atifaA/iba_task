from typing import List

class Solution:
    def carParkingRoof(self, cars: List[int], k: int) -> float:
        if k == 1:
            return 1 #if only 1 one car exists, 1 roof is enough
        cars = sorted(cars)
        roof_size = float('inf') #initializing it to the max number possible, since the purpose is to find the min
        for i in range(len(cars) - k + 1): #to check every position
            roof_size = min(roof_size, cars[i + k - 1] - cars[i] + 1) #get the minimum roof_size
        return roof_size


if __name__ == "__main__":
    cars: List[int] = [6, 2, 12, 7]
    k: int = 3
    print("A minimum roof size to cover the given cars is: ",Solution().carParkingRoof(cars,3))